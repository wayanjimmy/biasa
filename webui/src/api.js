import ky from 'ky'

const api = ky.extend({
  prefixUrl: 'http://localhost:9090/api'
})

export default api
