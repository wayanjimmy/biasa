import Vue from 'vue'
import Chakra, { CThemeProvider, CReset } from '@chakra-ui/vue'

import App from './App.vue'
import router from './router'

Vue.use(Chakra)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(CThemeProvider, [h(CReset), h(App)])
}).$mount('#app')
