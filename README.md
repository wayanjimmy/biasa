# Biasa

Personal habit racker written in Go

### Backend

- [ ] Habit CRUD
- [ ] Cue CRUD
- [ ] Reward CRUD
- [ ] Audit log
- [ ] i18n

### Frontend

- [ ] Habit CRUD
- [ ] Habit Search
- [ ] i18n

## Links

- [Bundling templates with pkger](https://osinet./go/en/articles/bundling-templates-with-pkger/)
