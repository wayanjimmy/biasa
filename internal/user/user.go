package user

import (
	"context"
)

type Module struct {
}

type User struct {
	Email string
	Name  string
}

type Repository interface {
	Create(ctx context.Context, data User) error
	FindByEmail(ctx context.Context, email string) (User, error)
}
