package cue

import (
	"context"
)

type Cue struct {
	Name string
}

type Module struct {
	repo Repository
}

type Repository interface {
	Init() error
	Create(ctx context.Context, data Cue) (int, error)
	All(ctx context.Context) ([]Cue, error)
}
