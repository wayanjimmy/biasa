package db

import (
	"time"

	"github.com/boltdb/bolt"
)

type Module struct {
	*bolt.DB
	driver string
	dsn    string
}

func New(driver, dsn string) (*Module, error) {
	db, err := bolt.Open(dsn, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return nil, err
	}

	m := &Module{
		db,
		driver,
		dsn,
	}

	return m, m.init()
}

func (m *Module) init() (err error) {
	switch m.driver {
	case "bolt":
		err = m.Update(func(tx *bolt.Tx) error {
			return nil
		})
	}
	return
}
