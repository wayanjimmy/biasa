package setting

import (
	"context"
	"errors"
)

type Module struct {
	repo Repository
}

type Repository interface {
	Store(ctx context.Context, setting Setting) error
	Get(ctx context.Context) (Setting, error)
}

type Setting struct {
	Email     string
	Installed bool
}

func New(repo Repository) *Module {
	return &Module{
		repo: repo,
	}
}

func (s *Setting) Validate() error {
	if s.Email == "" {
		return errors.New("setting: email is empty")
	}

	return nil
}

func (m *Module) Store(ctx context.Context, setting Setting) error {
	if err := setting.Validate(); err != nil {
		return err
	}

	err := m.repo.Store(ctx, setting)
	if err != nil {
		return err
	}

	return nil
}

func (m *Module) Get(ctx context.Context) (Setting, error) {
	return m.repo.Get(ctx)
}
