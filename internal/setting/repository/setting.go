package repository

import (
	"context"
	"encoding/json"

	"github.com/boltdb/bolt"
	"github.com/wayanjimmy/biasa/internal/db"
	"github.com/wayanjimmy/biasa/internal/setting"
)

var _ setting.Repository = (*Repository)(nil)
var settingBucket = []byte("setting")

type Repository struct {
	DB *db.Module
}

type Setting struct {
	Email     string `json:"email"`
	Installed bool   `json:"installed"`
}

func (r *Repository) Store(ctx context.Context, setting setting.Setting) error {
	err := r.DB.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(settingBucket)
		if err != nil {
			return err
		}

		b := tx.Bucket(settingBucket)
		s := Setting{
			Email:     setting.Email,
			Installed: setting.Installed,
		}

		encodedRecord, err := json.Marshal(s)
		if err != nil {
			return err
		}

		return b.Put([]byte(setting.Email), encodedRecord)
	})
	return err
}

func (r *Repository) Get(ctx context.Context) (setting.Setting, error) {
	result := setting.Setting{}

	err := r.DB.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(settingBucket)
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			var setting Setting
			err := json.Unmarshal(v, &setting)
			if err != nil {
				return err
			}

			result.Email = setting.Email
			result.Installed = setting.Installed
		}

		return nil
	})

	if err != nil {
		return result, err
	}
	return result, nil
}
