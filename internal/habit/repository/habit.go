package repository

import (
	"context"
	"encoding/json"

	"github.com/wayanjimmy/biasa/internal/bytes"

	"github.com/boltdb/bolt"
	"github.com/wayanjimmy/biasa/internal/db"
	"github.com/wayanjimmy/biasa/internal/habit"
)

var _ habit.Repository = (*Repository)(nil)
var habitBucket = []byte("habits")

// Repository ...
type Repository struct {
	DB *db.Module
}

// Habit ...
type Habit struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Create ...
func (r *Repository) Create(ctx context.Context, data habit.Habit) (int, error) {
	var id int
	err := r.DB.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(habitBucket)
		if err != nil {
			return err
		}

		b := tx.Bucket(habitBucket)
		id64, _ := b.NextSequence()
		id = int(id64)
		key := bytes.Itob(id)

		habit := Habit{
			ID:   id,
			Name: data.Name,
		}

		encodedRecord, err := json.Marshal(habit)
		if err != nil {
			return err
		}

		return b.Put(key, encodedRecord)
	})
	if err != nil {
		return -1, err
	}

	return id, nil
}

// All ...
func (r *Repository) All(ctx context.Context) ([]habit.Habit, error) {
	var habits []Habit
	var result []habit.Habit
	err := r.DB.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(habitBucket)
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			var habit Habit
			err := json.Unmarshal(v, &habit)
			if err != nil {
				// TODO: pass to a log handler?
				continue
			}

			habits = append(habits, Habit{
				ID:   habit.ID,
				Name: habit.Name,
			})
		}
		return nil
	})
	if err != nil {
		return result, err
	}

	for _, h := range habits {
		result = append(result, habit.Habit{
			ID:   h.ID,
			Name: h.Name,
		})
	}

	return result, nil
}
