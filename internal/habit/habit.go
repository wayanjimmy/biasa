package habit

import (
	"context"
	"errors"
)

type Module struct {
	repo Repository
}

type Repository interface {
	Create(ctx context.Context, data Habit) (int, error)
	All(ctx context.Context) ([]Habit, error)
}

type Habit struct {
	ID   int
	Name string
}

func New(repo Repository) *Module {
	m := Module{
		repo: repo,
	}

	return &m
}

func (h *Habit) Validate() error {
	if h.Name == "" {
		return errors.New("habit: name is empty")
	}

	return nil
}

func (m *Module) Create(ctx context.Context, habit Habit) (int, error) {
	if err := habit.Validate(); err != nil {
		return -1, err
	}

	id, err := m.repo.Create(ctx, habit)
	if err != nil {
		return -1, err
	}

	return id, nil
}

func (m *Module) All(ctx context.Context) ([]Habit, error) {
	return m.repo.All(ctx)
}
