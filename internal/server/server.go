package server

import (
	"net/http"

	"github.com/wayanjimmy/biasa/internal/setting"

	"github.com/markbates/pkger"
	"github.com/wayanjimmy/biasa/internal/habit"

	"github.com/gorilla/mux"
)

// Server ...
type Server struct {
	habit   *habit.Module
	setting *setting.Module
	router  *router
}

func (s *Server) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	s.router.router.ServeHTTP(writer, request)
}

// NewServer ...
func NewServer(habitModule *habit.Module, settingModule *setting.Module) *Server {
	router := &router{
		router: mux.NewRouter(),
	}

	srv := &Server{
		habit:   habitModule,
		setting: settingModule,
		router:  router,
	}

	srv.routes()

	return srv
}

func (s *Server) routes() {
	dir := pkger.Include("/webui/dist")

	s.router.Get("/api/habits", s.cors(s.handleIndexHabit()))
	s.router.Post("/api/habits", s.cors(s.handleStoreHabit()))
	s.router.Get("/api/setting", s.cors(s.handleGetSetting()))
	s.router.Post("/api/setting", s.cors(s.handleStoreSetting()))
	s.router.router.PathPrefix("/").Handler(http.FileServer(pkger.Dir(dir)))
}
