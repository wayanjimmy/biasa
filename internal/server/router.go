package server

import (
	"net/http"

	"github.com/gorilla/mux"
)

type router struct {
	router *mux.Router
}

func (r *router) Get(path string, handler http.HandlerFunc) {
	route := r.router.NewRoute()
	method := http.MethodGet
	route.Methods(method)
	route.Path(path)
	route.HandlerFunc(handler)
}

func (r *router) Post(path string, handler http.HandlerFunc) {
	route := r.router.NewRoute()
	route.Methods(http.MethodPost, http.MethodOptions)
	route.Path(path)
	route.HandlerFunc(handler)
}
