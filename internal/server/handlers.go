package server

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/wayanjimmy/biasa/internal/setting"

	"github.com/wayanjimmy/biasa/internal/habit"
)

type habitReq struct {
	Name string `json:"name"`
}

type habitRes struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type settingReq struct {
	Email     string `json:"email"`
	Installed bool   `json:"installed"`
}

type settingRes struct {
	Email     string `json:"email"`
	Installed bool   `json:"installed"`
}

func (s *Server) cors(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h(w, r)
	}
}

func (s *Server) handleStoreHabit() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ctx context.Context
		var data habitReq

		reqBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "error parsing request", http.StatusBadRequest)
			return
		}

		err = json.Unmarshal(reqBody, &data)
		if err != nil {
			http.Error(w, "something went wrong", http.StatusBadRequest)
			return
		}

		id, err := s.habit.Create(ctx, habit.Habit{
			Name: data.Name,
		})
		if err != nil {
			http.Error(w, "error writing data to db", http.StatusInternalServerError)
			return
		}

		res := habitRes{
			ID:   id,
			Name: data.Name,
		}

		d, err := json.Marshal(res)

		w.Header().Add("Content-Type", "application/json")
		w.Write(d)
	}
}

func (s *Server) handleIndexHabit() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ctx context.Context
		var result []habitRes

		// TODO: add pagination

		habits, err := s.habit.All(ctx)
		if err != nil {
			fmt.Println(err)
			http.Error(w, "failed to fetch data from db", http.StatusInternalServerError)
			return
		}

		for _, habit := range habits {
			result = append(result, habitRes{
				ID:   habit.ID,
				Name: habit.Name,
			})
		}

		d, err := json.Marshal(result)
		if err != nil {
			http.Error(w, "something went wrong", http.StatusInternalServerError)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.Write(d)
	}
}

func (s *Server) handleStoreSetting() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ctx context.Context
		var data settingReq

		reqBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "error parsing request", http.StatusBadRequest)
			return
		}

		err = json.Unmarshal(reqBody, &data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// TODO: validate email

		setting := setting.Setting{
			Email:     data.Email,
			Installed: data.Installed,
		}

		err = s.setting.Store(ctx, setting)
		if err != nil {
			fmt.Println("error", err.Error())
			http.Error(w, "error writing data to db", http.StatusInternalServerError)
			return
		}

		res := settingRes{
			Email:     data.Email,
			Installed: data.Installed,
		}

		d, err := json.Marshal(res)
		if err != nil {
			http.Error(w, "something went wrong", http.StatusInternalServerError)
			return
		}

		w.Write(d)
	}
}

func (s *Server) handleGetSetting() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ctx context.Context
		var res settingRes

		setting, err := s.setting.Get(ctx)
		if err != nil {
			http.Error(w, "error writing data to db", http.StatusInternalServerError)
			return
		}

		res.Email = setting.Email
		res.Installed = false

		d, err := json.Marshal(res)
		if err != nil {
			http.Error(w, "something went wrong", http.StatusInternalServerError)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.Write(d)
	}
}
