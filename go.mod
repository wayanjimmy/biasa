module github.com/wayanjimmy/biasa

go 1.13

require (
	github.com/boltdb/bolt v1.3.1
	github.com/gorilla/mux v1.7.4
	github.com/markbates/pkger v0.17.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
)
