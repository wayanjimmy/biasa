//go:generate pkger
package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"

	habitrepo "github.com/wayanjimmy/biasa/internal/habit/repository"
	"github.com/wayanjimmy/biasa/internal/setting"
	settingRepo "github.com/wayanjimmy/biasa/internal/setting/repository"

	"github.com/mitchellh/go-homedir"
	"github.com/pkg/errors"
	"github.com/wayanjimmy/biasa/internal/db"
	"github.com/wayanjimmy/biasa/internal/habit"
	"github.com/wayanjimmy/biasa/internal/server"
)

// Flags contains app configurations
type Flags struct {
	Serve bool
}

func main() {
	f := Flags{}
	flag.BoolVar(&f.Serve, "serve", false, "flag to active web server")
	flag.Parse()

	exitCode := 0
	if err := run(f); err != nil {
		fmt.Printf("%v", err)
		exitCode = -1
	}
	os.Exit(exitCode)
}

func run(f Flags) error {
	// prepare database
	home, _ := homedir.Dir()
	dsn := filepath.Join(home, "biasa.db")
	db, err := db.New("bolt", dsn)
	if err != nil {
		return errors.Wrap(err, "setup database")
	}
	defer db.Close()

	// prepare repositories
	habitRepo := &habitrepo.Repository{
		DB: db,
	}
	settingRepo := &settingRepo.Repository{
		DB: db,
	}

	// prepare server
	habit := habit.New(habitRepo)
	setting := setting.New(settingRepo)
	srv := server.NewServer(habit, setting)
	addr := ":9090"

	if f.Serve {
		// run server
		fmt.Printf("Server listening at %s for HTTP requests...\n", addr)
		log.Fatalln(http.ListenAndServe(addr, srv))
	} else {
		fmt.Println("Hello world!")
	}

	return nil
}
